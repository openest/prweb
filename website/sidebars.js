module.exports = {
  docs: [
    {
      type: 'category',
      label: 'BIM',
      items: ['bim-temp'],
    },

    {
      type: 'category',
      label: 'IFC',
      items: ["ifc-temp", "IPFEntityAnalyzer", "syntax_highlight_exp-step", "misprinted1", "IFCPSETGenerator",],
    },

    {
      type: 'category',
      label: 'Numerical Analysis',
      items: ["na-temp",],
    },

    {
      type: 'category',
      label: 'About',
      items: ["welcomedoc", "publicationdoc", "contactdoc",],
    },


  ],
};
