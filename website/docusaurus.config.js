/** @type {import('@docusaurus/types').DocusaurusConfig} */
/* docusaurus를 이용해서 페이지를 만들면 기본 페이지가 소개 페이지로 나오는데, 바로 도큐먼트 페이지가 나오게 하기 위해서는
  1. /website/src/pages 의 index.js 지우기
  2. /website/static index.html 페이지 만들기
  의 순서로 진행해야 한다.
*/
module.exports = {
  title: 'My Site',
  //tagline: 'The tagline of my site',
  url: 'https://openest.gitlab.io',
  baseUrl: '/prweb/',
  onBrokenLinks: 'throw',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  //organizationName: '', // Usually your GitHub org/user name.
  projectName: 'PRWEB', // Usually your repo name.
    
  themeConfig: {
    colorMode: {
      defaultMode: 'dark',
    },
    
    googleAnalytics: {
      trackingID: 'UA-179398687-1',
    },

    navbar: {
      //title: 'Home',
      items: [
        {
          to: 'docs/',
          activeBasePath: 'docs',
          label: 'Home',
          position: 'left',
        },
        ],
    },
  },
  
  /* preset은 몇가지 Themes와 Plugins를 합쳐서 미리 만들어 놓은 것이다. https://docusaurus.io/docs/next/presets#docusauruspreset-classic
    따라서 세무 필드의 정의는 Plugin의 내용을 참조해도 될 듯하다. 근데 짜증나는 것은, 현재 showLastUpdateAuthor: true, showLastUpdateTime: true, 가 안된다는 것이다.
    아무리 찾아봐도 저 위치가 맞다.
    위에 보면 기본 theme을 dark로 지정해 놓았는데, 이것도 나중에 적용된 것으로 봐서 어쩌면 시간이 지나면 반영이 될것 같기도 하고......
    참고 사이트:
    https://docusaurus.io/docs/next/presets
    https://docusaurus.io/docs/migration/manual
  */
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          // Please change this to your repo.
          /*editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/',*/
          showLastUpdateAuthor: true,
          showLastUpdateTime: true,
        },
        /*blog: {
          showReadingTime: true,
          // Please change this to your repo.
          editUrl:
            'https://github.com/facebook/docusaurus/edit/master/website/blog/',
        },*/
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
        sitemap: {
          changefreq: 'weekly',
          priority: 0.8,
          trailingSlash: false,
        },
        
      },
    ],
  ],
};
