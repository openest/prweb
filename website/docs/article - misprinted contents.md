---
id: misprinted1
title: misprint - Extended IFC-based strong form meshfree collocation analysis of a bridge structure
description: correction of the misprinted contents
sidebar_label: correction of the misprinted contents
---

I am the first author of this article, <a href="https://doi.org/10.1016/j.autcon.2020.103364" target="_blank">Extended IFC-based strong form meshfree collocation analysis of a bridge structure</a>, and 
There are **two misprinted contents** of the article.

## Fig. 1
PDF file (misprinted)
![misprint1-fig1](assets/misprint1-fig1.png)
⬇︎
Correct figure
![correction-misprint1-fig1](assets/correction-misprint1-fig1.png)

## Table 2
PDF file (misprinted)
![misprint2-table2](assets/misprint2-table2.png)
⬇︎
Correct table
![correction-misprint2-table2](assets/correction-misprint2-table2.png)


