---
id: IPFEntityAnalyzer
title: IPFEntityAnalyzer Beta 5
description: Introducing the IPFEntityAnalyzer Beta 5. IFC, Industry Foundation Classes, IfcBridge, STEP, XML, EXPRESS, IFC entity analyze, ST-Developer, ISO 10303, BIM, IFC analyze, java, IPF.
sidebar_label: IPFEntityAnalyzer Beta 5
---

![IPFEntityAnalyzer-Beta4](assets/IPFEntityAnalyzer-Beta4.png)

<iframe width="560" height="315" src="https://www.youtube.com/embed/jFECr5TLQA4" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

---

## Objectives
- IPFEntityAnalyzer was developed for personal research to quickly check the structure and data of IFC files.
- So, please ***use this only for personal research, and DO NOT redistribute*** this program.

## Feature
- The analyzed results - entity structure, attributes, and attribute data - are saved in a generated XML file using the IPF entity IDs or entity names.
- Basic conformance checking of the IPF syntax according to the IFC Schema.

## How to use
- **Currently, it supports IFC2X3, IFC4 add2, [IfcRailway ver. 0.7.5](https://openest.gitlab.io/ifcbridgeaddmeshfree_0p1/).**
- Select the schema type, import the IFC file, and enter the IDs or name of the entity you want to analyze.
- The comma (,) separates inputs of multiple items.
- To reduce the computing time and eliminate unnecessary entities in the output file, you can specify the entities' names to be excluded.
- The output file is generated as a "mapDoc.xml" file in the same folder as the IFC file you read.
- Run jar file in **root folder**

## Development Environment & Libraries
- Java 1.8.0_181
- Eclipse
- ST-Developer for Java Lib.
- WindowBuilder 1.9.3

## Bug reports
- E-mail.

## Download
- <a href="https://www.dropbox.com/t/0yDMp55SF5XODNR8" target="_blank">Click Here</a>
- Files
	- IPFEntityAnalyzer-Beta5.jar - Runnable jar.
	- sampleIFCFile - folder
	- simpleModel_ifc4.ifc - basic model.
	- simpleModel_ifc4_withError.ifc - basic model with intentional errors.
	- stbox_-simple-withoutBolt-Revit2018.ifc - simple bridge model.
	- simpleModel_IfcRailway.ifc - basic model based on the IFCRailway schema.
- size: 12MB

## Cite
- Park, S.I. (2020), IPFEntityAnalyzer Beta 5, Retrieved from https://openest.gitlab.io/prweb/docs/IPFEntityAnalyzer/. {your access date}

---

## 목적
- IFC 파일의 구조와 데이터를 쉽게 확인하기 위해 연구용으로 개발.
- 따라서 ***개인의 연구용으로만 사용하시길 바라며, 재배포를 금지합니다.***

## 기능
- IFC 엔티티의 식별 ID, or 엔티티 이름을 이용해서 그 구조와 속성의 이름, 속성 데이터를 XML 파일로 보여줌.
- 스키마에 따른 IPF의 기본적인 구문 체크.

## 사용 방법
- **현재 지원하는 버전은 IFC2X3, IFC4 add2, [IfcRailway ver. 0.7.5](https://openest.gitlab.io/ifcbridgeaddmeshfree_0p1/).**
- 스키마 종류를 선택하고, 파일을 불러온 후, 분석하고자 하는 엔티티의 ID 또는 엔티티의 이름을 넣어줌.
- 여러 엔티티의 입력은 콤마(,)를 통해 구분.
- 이때, 분석에 걸리는 시간을 줄이고, XML 파일 읽기에 불필요한 요소를 제거하기 위해, 분석 시 제외할 항목을 지정할 수 있음.
- 결과 파일은 읽어온 IFC 파일과 같은 폴더에 “mapDoc.xml” 파일로 생성.
- **root 폴더에서 jar 실행**


## 개발 환경 및 사용 라이브러리
- Java 1.8.0_181
- Eclipse
- ST-Developer forJava Lib.
- WindowBuider 1.9.3

## 버그 리포트
- 이메일.

## 다운로드
- <a href="https://www.dropbox.com/t/0yDMp55SF5XODNR8" target="_blank">Click Here</a>
- Files
	- IPFEntityAnalyzer-Beta5.jar - Runnable jar.
	- sampleIFCFile - folder
	- simpleModel_ifc4.ifc - basic model.
	- simpleModel_ifc4_withError.ifc - basic model with intentional errors.
	- stbox_-simple-withoutBolt-Revit2018.ifc - simple bridge model.
	- simpleModel_IfcRailway.ifc - basic model based on the IFCRailway schema.
- size: 12MB

## Cite
- Park, S.I. (2020), IPFEntityAnalyzer Beta 5, Retrieved from https://openest.gitlab.io/prweb/docs/IPFEntityAnalyzer/. {your access date}