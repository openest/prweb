---
id: publicationdoc
title: Publications
description: publications of journal articles and conference proceedings
sidebar_label: Publications
---

- <a href="https://scholar.google.com/citations?hl=en&user=W3QVD68AAAAJ&view_op=list_works" target="_blank">Google Scholar</a>