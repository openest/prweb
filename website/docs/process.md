---
id: process
title: for process
description: description
sidebar_label: Processing
---

⬆︎ 시작형식.

Docusaurus v2로 업데이트 되면서 몇가지가 변경됨.

## 웹페이지에 표시
- title이 제일 큰 제목으로 페이지의 상단에 나타남.

## md 파일 작성 팁
### code block
```mdx title
markdown
```

```jsx title
code...
```

```diff title
+ add
- minus
```

- 참고: 특정 줄 하이라이팅도 됨. 아래 사이트 참조.
- ref: [click](https://docusaurus.io/docs/markdown-features/code-blocks)

### admonition
- ⬇︎ 아래처럼 `:::` 사용하면 특별한 색의 박스가 생성됨.
```
:::note

The content and title *can* include markdown.

:::

:::tip You can specify an optional title

Heads up! Here's a pro-tip.

:::

:::info

Useful information.

:::

:::caution

Warning! You better pay attention!

:::

:::danger

Danger danger, mayday!

:::
```


## side bar에 표시
~~- website/sidebar.json 파일에서 id를 활용해서 함. ~~
- website/sidebar.js 파일에서 id를 활용해서 함. 파일 열어보면 이해 될 것임.
~~- 왼편 사이드바에 나타나는 이름은 sidebar_label임.~~
- sidebar는 sidebars.json에서 관리하는데, 안타깝게도 하나의 문서가 두개이상의 카테고리에 들어갈 수는 없음. -> 이건 v2에서는 아직 모르겠음.
- ref: [click](https://docusaurus.io/docs/sidebar)

## sitemap 추가
- 필요하면 sitemap에 추가. Screaming Frog SEO Spider 사용해도 될거고 (근데 이거 무료버전은 횟수나 crawling 제한이 있는 것 깉기도 하고, 아닌것 같기도 하고...), 직접 내용 추가해도 될거고.
- 기본형식
`<url><loc>https://openest.gitlab.io/prweb/docs/IPFEntityAnalyzer/</loc> <lastmod>2020-10-07</lastmod> <priority>0.9</priority> <image:image><image:loc>https://openest.gitlab.io/prweb/docs/assets/IPFEntityAnalyzer-Beta4.png</image:loc><image:title>IPFEntityAnalyzer-Beta4</image:title></image:image></url>`

- 즉, loc에는 실제 주소가 들어가는데, 이때, id를 활용한다. 즉, `https://openest.gitlab.io/prweb/docs/[id]]/`
- 이미지가 추가된 경우, 이미지 주소는 이미지 주소고, image의 title은 md 파일에서 image link 생성할때 사용한 title이다.
- sitemap.xml 파일이 들어갈 위치는 /Users/PLUTON/Dropbox/200_Personal/10_Webpage/website/static/sitemap.xml

## Outline 생성
- 페이지의 오른편에 나오는 글의 outline은 마크다운 헤더를 통해 **자동으로 인식.**
- 이때, 샵 2개를 가장 상위 제목으로 인식하며, 샵 3개까지 나타남.

## Image link
- 문서끼리 링크 말고, 이미지 등 추가 파일의 링크는 **"assets"** 라는 폴더에 넣어서 링크 시켜야함.
- docusaurus에서는 다른 이름 안됨. 반드시 assets 폴더.
- 예. ![IPFEntityAnalyzer-Beta4](assets/IPFEntityAnalyzer-Beta4.png)

## 새창에서 열기
`<a href="[주소]" target="_blank">[표시단어]</a>`

~~## 상단링크~~
~~- 상단링크 수정이 필요한 경우  siteConfig.js 도 수정해야 함.~~
