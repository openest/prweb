---
id: syntax_highlight_exp-step
title: Syntax Highlight of EXPRESS (.exp) and STEP (.stp, .step, .ifc) in Sublime Text app
description: Syntax Highlight of EXPRESS and STEP in Sublime Text app. IFC, Industry Foundation Classes, EXPRESS.
sidebar_label: Syntax Highlight of EXPRESS and STEP
---

![syntax-highlight](assets/syntax-highlight.png)

## How to use
1. Download zip file: <a href="https://www.dropbox.com/t/MG11XRRTRyQ4t22M" target="_blank">Click Here</a>
2. Unzip - It includes 4 files;
	- express.tmLanguage
	- express.YAML-tmLanguage
	- step.tmLanguage
	- step.YAML-tmLanguage
3. Setup 
	- Sublime Text > Preferences > Browse Packages...
	- Copy (or Move) 4 files to 'User' folder
4.  Enjoy.