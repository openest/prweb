---
id: IFCPSETGenerator
title: IFC-PSET Generator
description: Introducing of the IFC-PSET Generator. IFC, Industry Foundation Classes, STEP, IFC entity, ST-Developer, ISO 10303, BIM, java, IPF.
sidebar_label: IFC-PSET Generator
---

![IFC-PSET Generator.png](assets/IFC-PSET_Generator.png)

## Feature
- An application adding a user-defined property set to an IFC file.
- Support IFC4add2

## How to use
<iframe width="560" height="315" src="https://www.youtube.com/embed/3qVMYXT0n8Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## Development Environment & Libraries
- Java 1.8.0_181
- ST-Developer for Java Lib. 1.0
- IntelliJ IDEA

## Bug reports
- E-mail.

## Executable Jar Download
- <a href="https://l.linklyhq.com/l/ZKim" target="_blank">Download</a>
```
--- IFCPsetGenerator.jar - Executable Jar (5.9MB)
--- sampleIFCfiles (5.5MB)
 |
 |--- columnExample.ifc
 |
 |--- sampleFile-SaengTae6Bridge-IFC4.ifc
```

## Source codes
- <a href="https://l.linklyhq.com/l/ZN3v" target="_blank">Gitlab</a>


## Cite
- Park, S.I. (2021), IFC-PSET Generator, Retrieved from https://openest.gitlab.io/prweb/docs/IFCPSETGenerator/. {your access date}

---

## 기능
- IFC 파일에 user-defined property set을 추가하는 app.
- IFC4add2 지원.

## 사용법
<iframe width="560" height="315" src="https://www.youtube.com/embed/3qVMYXT0n8Y" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

## 개발 환경 및 사용 라이브러리
- Java 1.8.0_181
- ST-Developer for Java Lib. 1.0
- IntelliJ IDEA

## 버그 리포트
- E-mail.

## 실행 가능한 jar 파일 다운로드
- <a href="https://l.linklyhq.com/l/ZKim" target="_blank">다운로드</a>
```
--- IFCPsetGenerator.jar - Executable Jar (5.9MB)
--- sampleIFCfiles (5.5MB)
 |
 |--- columnExample.ifc
 |
 |--- sampleFile-SaengTae6Bridge-IFC4.ifc
```

## 소스코드
- <a href="https://l.linklyhq.com/l/ZN3v" target="_blank">Gitlab</a>


## Cite
- Park, S.I. (2021), IFC-PSET Generator, Retrieved from https://openest.gitlab.io/prweb/docs/IFCPSETGenerator/. {your access date}

---
Last update: Aug. 1, 2021.